import Vue from 'vue'
import { mapGetters } from 'vuex'

const Validation = {
  install(Vue, option) {
    Vue.mixin({
      computed: {
        ...mapGetters({
          errors: 'validations/errors'
        })
      }
    })
  }
}

Vue.use(Validation)
