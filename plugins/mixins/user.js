import Vue from 'vue'
import { mapGetters } from 'vuex'

const User = {
  install(Vue, option) {
    Vue.mixin({
      computed: {
        ...mapGetters({
          user: 'auth/user',
          authenticated: 'auth/authenticatedUser'
        })
      }
    })
  }
}

Vue.use(User)
