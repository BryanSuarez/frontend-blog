export default ({ $axios, store, redirect }) => {
  $axios.onError((error) => {
    if (error.response.status === 422) {
      store.dispatch('validations/setErrors', error.response.data.errors)
      return redirect('/login')
    }
    return Promise.reject(error)
  })

  $axios.onRequest(() => {
    store.dispatch('validations/clearErrors')
  })
}
