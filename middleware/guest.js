export default ({ store, redirect }) => {
  if (store.getters['auth/authenticatedUser']) {
    return redirect('/dashboard')
  }
}
