export const getters = {
  authenticatedUser(state) {
    return state.loggedIn
  },

  user(state) {
    return state.user
  }
}
