import { SET_VALIDATION_ERRORS } from './mutation_types'

export default {
  setErrors({ commit }, errors) {
    commit(SET_VALIDATION_ERRORS, errors)
  },
  clearErrors({ commit }) {
    commit(SET_VALIDATION_ERRORS, {})
  }
}
