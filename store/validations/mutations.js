import { SET_VALIDATION_ERRORS } from './mutation_types'

export default {
  [SET_VALIDATION_ERRORS](state, errors) {
    state.errors = errors
  }
}
